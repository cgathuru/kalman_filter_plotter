import sys

import numpy as np
import sympy
from sklearn import linear_model
from statistics import median
import statistics


def main():
    filename = sys.argv[1]
    # x, y, z = np.loadtxt(filename, delimiter=' ', usecols=(0, 1, 2), unpack=True)
    zbd = np.loadtxt("z_b-down.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)
    zbu = np.loadtxt("z_b-up.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)
    ybd = np.loadtxt("y_b-down.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)
    ybu = np.loadtxt("y_b-up.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)
    xbd = np.loadtxt("x_b-down.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)
    xbu = np.loadtxt("x_b-up.txt", delimiter=' ', usecols=(0, 1, 2), unpack=False)

    # items_z = [zbd, zbu]
    # matrix_z = map(create_matrix_z, items_z)
    # items_y = [ybd, ybu]
    # matrix_y = map(create_matrix_y, items_y)
    # items_x = [xbd, xbu]
    # matrix_x = map(create_matrix_x, items_x)
    # matrix = list(matrix_z) + list(matrix_y) + list(matrix_x)
    # acc = np.array(matrix)
    # print(acc)
    # print(np.linalg.norm(acc))
    # print(np.dot(np.array([1, 1, 1, 1]), acc))

    # zbd = np.array(zbd)
    zbd = np.array(np.apply_along_axis(divide, 0, zbd))
    # print(zbd)
    w1 = add_ones_column(zbd)
    y1 = make_y_array([0, 0, 1], zbd)

    # zbu = np.array(zbu)
    zbu = np.array(np.apply_along_axis(divide, 0, zbu))
    # print(zbu)
    w2 = add_ones_column(zbu)
    y2 = make_y_array([0, 0, -1], zbu)

    # ybd = np.array(ybd)
    ybd = np.array(np.apply_along_axis(divide, 0, ybd))
    # print(ybd)
    y3 = make_y_array([0, 1, 0], ybd)
    w3 = add_ones_column(ybd)

    # ybu = np.array(ybu)
    ybu = np.array(np.apply_along_axis(divide, 0, ybu))
    # print(ybu)
    y4 = make_y_array([0, -1, 0], ybu)
    w4 = add_ones_column(ybu)

    # xbd = np.array(xbd)
    xbd = np.array(np.apply_along_axis(divide, 0, xbd))
    # print(xbd)
    y5 = make_y_array([1, 0, 0], xbd)
    w5 = add_ones_column(xbd)

    # xbu = np.array(xbu)
    xbu = np.array(np.apply_along_axis(divide, 0, xbu))
    # print(xbu)
    y6 = make_y_array([-1, 0, 0], xbu)
    w6 = add_ones_column(xbu)

    w = w1
    y = y1
    w = np.append(w1, w2, axis=0)
    matrices = [w3, w4, w5, w6]
    w = append_matrices(matrices, w)

    y = np.append(y1, y2, axis=0)
    matrices = [y3, y4, y5, y6]
    y = append_matrices(matrices, y)

    wt = np.transpose(w)
    x = np.dot(wt, w)
    x = np.linalg.inv(x)
    x = np.dot(x, wt)

    x = np.dot(x, y)

    print(x)

    a = sympy.MatrixSymbol('a', 3, 1)

    a = sympy.Matrix(a)

    constants = x[-1, :]
    constants = sympy.Matrix(constants)

    x = sympy.Matrix(x[0:-1, :])
    xa = sympy.Matrix(x.dot(a))

    solution = xa + constants
    print(solution.shape)
    for row in solution:
        print(row)

    # print(solution)

    # simulation = map(get_calibrated_value, zbd)
    # simulation = [get_calibrated_value_from_matrix(values, solution, a) for values in zbd]
    #for index, sim in enumerate(simulation):
       # print(sim, zbd[index], )


def divide(number):
    # return number / 1000 if abs(median(number)) > 900 else number
    return number/1000


def append_matrices(matrices, start_matrix):
    w = start_matrix
    for matrix in matrices:
        w = np.append(w, matrix, axis=0)
    return w


def make_y_array(vector, data):
    return np.array([np.array(vector), ] * data.shape[0])


def add_ones_column(zbd):
    return np.c_[zbd, np.ones(zbd.shape[0])]


def create_matrix_z(data):
    clf = linear_model.LinearRegression()
    zbd_x = np.array(data[:, [0, 1]])
    zbd_y = np.array(data[:, 2])
    clf.fit(zbd_x, zbd_y)
    display = list(clf.coef_)
    # display.append(clf.intercept_)
    return display


def create_matrix_y(data):
    clf = linear_model.LinearRegression(normalize=True)
    zbd_x = np.array(data[:, [0, 2]])
    zbd_y = np.array(data[:, 1])
    clf.fit(zbd_x, zbd_y)
    display = list(clf.coef_)
    # display.insert(1, clf.intercept_)
    return display


def create_matrix_x(data):
    clf = linear_model.LinearRegression()
    zbd_x = np.array(data[:, [1, 2]])
    zbd_y = np.array(data[:, 0])
    clf.fit(zbd_x, zbd_y)
    display = list(clf.coef_)
    # display.insert(0, clf.intercept_)
    return display


def get_calibrated_value(values):
    x = -0.0307763741668983 + 0.892063144562425*values[0] - 0.0135889188095689*values[1] + 0.0511914786252098*values[2]
    y = -0.0154374235846891 + 0.00502357243789051*values[0] + 0.983969365749847*values[1] - 0.0409409204996511*values[2]
    z = -0.00872978596497249 - 0.0227956151737385*values[0] + 0.031850235067119*values[1] + 0.993905635460674*values[2]
    return x, y, z


def get_calibrated_value_from_matrix(values, cal_matrix, symbols):
    # index_map = {symbols[0, 0]: values[0], symbols[1, 0]: values[1], symbols[2, 0]: values[2]}
    a = sympy.MatrixSymbol('a', 3, 1)

    a = sympy.Matrix(a)
    cal_matrix = cal_matrix.subs(a[0, 0], values[0])
    cal_matrix = cal_matrix.subs(a[1, 0], values[1])
    cal_matrix = cal_matrix.subs(a[2, 0], values[2])

    return cal_matrix


if __name__ == '__main__':
    main()
