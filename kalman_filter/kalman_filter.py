import matplotlib.pyplot as plt
import sys


def main():
    filename = sys.argv[1]
    data = []
    filtered = []
    kalman_filter = KalmanFilter(q=0.01, r=0.100, p=2)
    with open(filename, mode='r') as infile:
        for line in infile:
            data.append(float(line))
            print(line)
            filtered.append(kalman_filter.update(float(line)))
        else:
            filtered = filtered[0:500]
            data = data[0:500]
    plt.plot(data, 'r', label='Temperature')
    plt.plot(filtered, 'b', label='Filtered Temperature\n' + 'q=' + str(kalman_filter.q) + ' r=' + str(kalman_filter.r))
    plt.legend(loc='upper right')
    plt.ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
    plt.xlabel('Number of Samples')
    plt.title('Temperature vs Number of Samples')
    plt.show()


class KalmanFilter(object):
    k = 0.0

    def __init__(self, q=0.1, r=0.1, p=0.1, initial_value=16):
        self.q = q
        self.r = r
        self.p = p
        self.x = initial_value

    def update(self, measurement):
        self.p += self.q
        self.k = self.p / (self.p + self.r)
        self.x += self.k * (measurement - self.x)
        self.p *= (1 - self.k)

        return self.x

if __name__ == '__main__':
    main()
